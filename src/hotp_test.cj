package otp

import crypto.digest.HashType
import std.unittest.*
import std.unittest.testmacro.*
import std.math
import std.random.Random

@Test
public class HotpTest {
    @TestCase[
        args in [
            (0, "755224"),
            (1, "287082"),
            (2, "359152"),
            (3, "969429"),
            (4, "338314"),
            (5, "254676"),
            (6, "287922"),
            (7, "162583"),
            (8, "399871"),
            (9, "520489")
        ]
    ]
    public func testSha1(args: (Int64, String)) {
        let (counter, result) = args
        // secret for HMAC-SHA1 - 20 bytes
        let secret = "3132333435363738393031323334353637383930"
        let hotp = Hotp.builder().setAlgorithm(HashType.SHA1).setDigits(result.size).setSecretDecode(hexDecodeTest).build()
        @Expect(hotp.generate(secret, counter), result)
    }

    @TestCase[
        args in [
            (100, "02951655"),
            (1000000000000, "29669975")
        ]
    ]
    public func testChecksum(args: (Int64, String)) {
        let (counter, result) = args
        // secret for HMAC-SHA1 - 20 bytes
        let secret = "3132333435363738393031323334353637383930"
        let hotp = Hotp.builder().setChecksum().setDigits(result.size - 1).setSecretDecode(hexDecodeTest).build()
        @Expect(hotp.generate(secret, counter), result)
    }

    @TestCase[
        args in [
            (100, "62983787"),
            (1000000000000, "05612759")
        ]
    ]
    public func testTruncationOffset(args: (Int64, String)) {
        let (counter, result) = args
        // secret for HMAC-SHA1 - 20 bytes
        let secret = "3132333435363738393031323334353637383930"
        let hotp = Hotp.builder().setTruncationOffset(3).setDigits(result.size).setSecretDecode(hexDecodeTest).build()
        @Expect(hotp.generate(secret, counter), result)
    }

    @TestCase
    public func testCodeDigits() {
        let hotp = Hotp.builder().build()
        @Expect(hotp.codeDigits, 6)
        let hotp2 = Hotp.builder().setDigits(7).setChecksum(true).build()
        @Expect(hotp2.codeDigits, 8)
    }

    @TestCase
    public func testVerify() {
        let secret = "1234567890"
        let hotp = Hotp.builder().build()
        @Expect(hotp.verify(secret, 1024, "879473"), true)
        @Expect(hotp.verify(secret, 1024, "879000"), false)
    }
}

@Test
public class HotpBench {
    private let secret = "1234567890"
    private let counters: Array<Int64>
    private let hotp = Hotp.builder().build()
    public HotpBench() {
        counters = generateData() 
    }

    static func generateData() {
        let random = Random(0)
        let max = Int64(Int32.Max) << 2
        return Array<Int64>(100000) {
            _ =>
            math.abs(random.nextInt64(max))
        }
    }

    @Bench 
    public func simpleBench() {
        for (counter in counters) {
            hotp.generate(secret, counter)
        }
    }
}