package otp

import crypto.digest.HashType
import std.convert.Parsable
import std.collection.concat
import std.time.DateTime

/**
 * 基于时间的一次性密码
 *
 * @see <a href="https://tools.ietf.org/html/rfc6238">RFC 6238</a>
 *
 * @author devinx3
 */
public class Totp <: Otp {
    private let algorithm: HashType
    private let secretDecode: SecretDecoder
    private let period: Int64
    private let digits: Int64

    Totp(builder: TotpBuilder) {
        this.algorithm = builder.algorithm
        this.secretDecode = builder.secretDecode
        this.period = builder.period
        this.digits = builder.digits
    }

    public static func builder() {
        return TotpBuilder()
    }

    public override prop codeDigits: Int64 {
        get() {
            this.digits
        }
    }

    /**
     * 基于当前时间生成一次性密码
     */
    public func now(secret: String) {
        return at(secret, DateTime.now())
    }

    /**
     * 基于指定时间生成一次性密码
     */
    public func at(secret: String, time: DateTime) {
        return at(secret, time.toUnixTimeStamp())
    }

    public func at(secret: String, duration: Duration) {
        return generate(secret, duration.toSeconds())
    }

    public override func generate(secret: String, duration: Int64): String {
        let counter = to8Bytes(duration / period)
        // 基于密钥和计数器生成 HMAC 散列
        let hash = hmacSha(this.algorithm, this.secretDecode(secret), counter)
        // 根据最后一个字节, 计算截取偏移量
        let offset = Int64(hash[hash.size - 1]) & 0xf

        // 生成 4 字节整数 (首位清除符号)
        let binary = ((Int64(hash[offset]) & 0x7f) << 24) | 
            ((Int64(hash[offset + 1]) & 0xff) << 16) | 
            ((Int64(hash[offset + 2]) & 0xff) << 8) |
            (Int64(hash[offset + 3]) & 0xff)
        // 截取指定位数
        let otp = binary % DIGITS_POWER[this.codeDigits]
        return otp.toString().padStart(this.codeDigits, padding: "0")
    }
}

/**
 * HOTP 算法建造者
 *
 * @author devinx3
 */
public class TotpBuilder {
    TotpBuilder() {}
    /**
     * SHA 算法类型
     */
    var algorithm = HashType.SHA1
    /**
     *  secret 解码方式
     */
    var secretDecode: SecretDecoder = defaultSecretDecode
    /**
     * 密码位数(默认6位)
     */
    var digits = 6
    /**
     * 时间间隔(默认30秒)
     */
    var period = 30

    public func setAlgorithm(algorithm: HashType) {
        this.algorithm = algorithm
        return this
    }

    public func setSecretDecode(secretDecode: SecretDecoder) {
        this.secretDecode = secretDecode
        return this
    }

    public func setPeriod(period: Int64) {
        this.period = period
        return this
    }

    public func setDigits(digits: Int64) {
        if (digits < 4 && digits > 8) {
            throw IllegalArgumentException("digits must be between 4 and 8")
        }
        this.digits = digits
        return this
    }

    public func build() {
        return Totp(this)
    }
}
