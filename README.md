## 介绍

otp 是一个仓颉库，用于生成 [HOTP（RFC 4226）](https://tools.ietf.org/html/rfc4226) 或 [TOTP（RFC 6238）](https://tools.ietf.org/html/rfc6238)一次性密码


## 快速开始
### 添加依赖
在 `cjpm.toml` 添加以下配置  
```toml
[dependencies]
  otp = { git = "https://gitcode.com/devinx3/otp" }
```


### 使用 TOTP

创建 TOTP 生成器

```cangjie
// 具有默认密码长度6位数字、时间步长（30秒）和HMAC算法（HMAC-SHA1）
var totp = Totp.builder().build()
// 设置指定配置的 TOTP 生成器
totp = Totp.builder()
  // 设置密码位数(4-8)
  .setDigits(8)
  // 设置时间步长
  .setPeriod(60)
  // 设置HMAC算法（HMAC-SHA1/HMAC-SHA256/HMAC-SHA512）
  .setAlgorithm(HashType.SHA512)
  // 设置密钥解码器
  .setSecretDecode {secret => secret.toArray()}
  .build()
```

生成基于时间的一次性密码

```cangjie
// 基于时间间隔生成
totp.generate("1234567890", 1735689600)
totp.at("1234567890", 28928160 * Duration.minute)
// 基于当前时间生成
totp.now("1234567890")
totp.at("1234567890", DateTime.now())
// 验证
totp.verify("1234567890", 1735689600, "123456")
```


### 使用 HOTP
创建 HOTP 生成器
```cangjie
// 具有默认密码长度6位数字和HMAC算法（HMAC-SHA1）
var hotp = Hotp.builder().build()
// 设置指定配置的 Hotp 生成器
hotp = Hotp.builder()
  // 设置密码位数(4-8)(不包含校验和)
  .setDigits(8)
  // 启用校验和
  .setChecksum(true)
  // 设置HMAC算法（HMAC-SHA1/HMAC-SHA256/HMAC-SHA512）
  .setAlgorithm(HashType.SHA512)
  // 设置密钥解码器
  .setSecretDecode {secret => secret.toArray()}
  // 设置截位首位索引(0-15)
  .setTruncationOffset(10)
  .build()
```

生成基于 HMAC 的一次性密码

```cangjie
// 生成
totp.generate("1234567890", 1024)
// 验证
totp.verify("1234567890", 1024, "123456")
```


## 开源协议

本项目基于 [MIT License](./LICENSE)


## 参与贡献

欢迎给我们提交PR，欢迎给我们提交Issue，欢迎参与任何形式的贡献。

